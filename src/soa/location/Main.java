package soa.location;

import com.lavasoft.GeoIPService;
import com.lavasoft.GeoIPServiceSoap;

public class Main {

    public static void main(String[] args) {
        GeoIPServiceSoap geoIPServiceSoap = new GeoIPService().getGeoIPServiceSoap();
        String geoIp = geoIPServiceSoap.getIpLocation("212.58.251.195");
        System.out.println(geoIp);
    }
}