package soa.location;

public class Example implements First, Second {

    private Runnable r;

    private int h;

    public Runnable getR() {
        return r;
    }

    public static void main(String[] args) {
        Example ex = new Example();
        ex.def();

        Thread t = new Thread(ex::getR);
    }

    public void def() {
        int i = 0;
        r = () -> {
            h++;
            System.out.println("excuse, me! " + h);
        };
        Thread t = new Thread(r);
        t.start();
    }
}
